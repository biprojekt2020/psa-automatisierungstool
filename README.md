# Readme
## Installation of the requirements
You need to have [Python>=3](https://www.python.org/downloads/) installed.
1. Create a virtual environment named `venv`:  
`python -m venv venv`
2. Activate the virtual environment with:
     - For Linux:  
        `source venv/bin/activate`
     - For Windows:
        `venv\Scripts\activate.bat`
     - More information  visit [the official venv page](https://docs.python.org/3/library/venv.html)

3. Install the requirements in the virtual environment:  
```python -m pip install -r requirements.txt```

## Set the run Configuration in PyCharm
Set a new configuration in PyCharm with the following value:

- Script path: `$PATH_TO_THE_PROJET_FOLDER\flaskr\app.py`
- Python interpreter: select the interpreter in the previously created virtual environment
- Working directory: `$PATH_TO_THE_PROJET_FOLDER\flaskr`
