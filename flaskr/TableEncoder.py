from json import JSONEncoder
from table import Table


# Diese Klasse ist ein Custom JSONEncoder für die Klasse Table.
# Damit lässt sich mit der vererbten Methode encode() ein JSON-String von diesem Objekt zurückgeben
class TableEncoder(JSONEncoder):

    def default(self, object):
        # Wenn das Objekt ein Table-Objekt ist
        if isinstance(object, Table):
            columns = {}
            # Jede Spalte als Dictionary dem "columns"-Dictionary hinzufügen
            for c in object.columns:
                columns[c] = object.columns.get(c).__dict__
            return {"name": object.name, "columns": columns, "isSelected": object.isSelected}

        # Wenn das Objekt KEIN Table-Objekt ist
        else:
            # Standard-Encoder verwenden
            return JSONEncoder.default(self, object)
