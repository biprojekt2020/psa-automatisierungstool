-- Damit zu jedem Ladelauf entsprechende Metadaten gesammelt und persistiert werden, müssen Informationen aus Snowflake extrahiert werden.
-- Diese Informationen befinden sich im INFORMATION_SCHEMA des jeweiligen WAREHOUSES. Es müssen entsprechend Filter definiert werden (Warehouse, Zeitinterval, Schema).
-- Alle selektierten Informationen des INFORMATION_SCHEMA werden mit Hilfe eines INSERT INTO in die Tabelle CF_LANDING_ZONE_META geschrieben und per Batch-Skript ausgeführt.

	insert into CONTROL_FRAMEWORK.CF_LANDING_ZONE_META (
		select QUERY_ID,
       		QUERY_TEXT,
       		DATABASE_NAME,
      		SCHEMA_NAME,
       		QUERY_TYPE,
       		SESSION_ID,
       		USER_NAME,
       		WAREHOUSE_NAME,
       		QUERY_TAG,
       		EXECUTION_STATUS,
       		ERROR_CODE,
       		ERROR_MESSAGE,
       		START_TIME,
       		END_TIME,
       		TOTAL_ELAPSED_TIME/100,
       		ROWS_PRODUCED,
       		EXECUTION_TIME,
       		CREDITS_USED_CLOUD_SERVICES
	from table (information_schema.query_history_by_warehouse(WAREHOUSE_NAME => 'STUD_FH_WH'
    			, END_TIME_RANGE_START => to_timestamp_ltz(current_date - interval '6d')
    			, END_TIME_RANGE_END => to_timestamp_ltz(current_date)
    			, RESULT_LIMIT => 100))
 	where SCHEMA_Name = 'LANDING_ZONE'
 	order by start_time;