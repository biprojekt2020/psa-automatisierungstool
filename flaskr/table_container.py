from table import Table
from TableEncoder import TableEncoder
import json, os

# Die Klasse TableContainer ist eine Containerklasse zur Datenverarbeitung der Tabelleninformationen in der Webanwendung
class TableContainer:
    tables = {}

    def __init__(self):
        self.tables = {}

    # Methode zum Hinzufügen einer Tabelle zu dem Container
    def addTable(self, table):
        if self.tables.get(table.name) is None:
            self.tables[table.name] = table

    # Methode zum Finden und Zurückgeben einer Tabelle anhand ihres Namens
    def findTable(self, name):
        if self.tables.get(name) is None:
            self.addTable(Table(name))
        return self.tables.get(name)

    # Methode zum Zurücksetzen der Tabellen-Auswahl des Nutzers für jede Tabelle des Containers
    # Spaltenauswahl bleibt bestehen ansonsten table.resetSelection() hinzufügen
    def resetSelection(self):
        for table in self.tables.values():
            table.deselect()

    # Methode, die eine Liste der aktuell ausgewählten Tabellen zurückgibt
    def getSelectedTables(self):
        t = []
        for table in self.tables.values():
            if table.isSelected:
                t.append(table)
        return t

    # Methode zum Leeren des Containers
    def clear(self):
        self.tables.clear()

    # Methode zum Schreiben des Containers als JSON-Files
    def writeJSON(self):
        # für jede Tabelle im Container
        for table in self.tables:
            # JSON-String generieren
            jsonString = TableEncoder().encode(self.findTable(table))
            # Schreiben der JSON-Strings in JSON-Files
            with open("./tables/" + table + ".json", "w+") as f:
                f.write(jsonString)

    # Methode zum Einlesen der JSON-Files in den Container
    def readJSON(self):
        # Zurücksetzen des Container
        self.clear()
        # Definieren des Pfades der JSON-Files
        directory = os.fsencode("./tables/")
        # Iterieren durch jedes File in dem definierten Pfad
        for file in os.listdir(directory):
            # Decoden des Filenames aus dem Pfad als String
            filename = os.fsdecode(file)
            # Nur Files verarbeiten, die auf .json enden
            if filename.endswith(".json"):
                with open(os.fsdecode(directory) + filename, "r") as f:
                    # JSON-String einlesen
                    jsonString = f.read()
                    # Payload-Objekt aus dem JSON-String erzeugen
                    payload = Payload(jsonString)
                    # Daten aus dem Payload-Objekt auslesen
                    tablename = payload.name
                    columns = payload.columns
                    isSelected = payload.isSelected

                    # Einlesen der Tables in die Datenstruktur
                    # Für jede Spalte in der Liste
                    for c in columns.values():
                        # Tabelle finden (wenn nicht vorhanden Erstellen) und Spalte hinzufügen
                        self.findTable(tablename).addColumn(c["name"], c["datatype"], c["length"], c["nullable"], c["isKey"], c["isSourceKey"], c["isSelected"])
                    # Tabellen-Auswahl setzen
                    self.findTable(tablename).isSelected = isSelected


# Klasse, welche ein Objekt aus einem JSON-File erstellt
class Payload(object):
    def __init__(self, j):
        self.__dict__ = json.loads(j)
