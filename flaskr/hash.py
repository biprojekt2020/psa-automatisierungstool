from jinja2 import Environment, FileSystemLoader

# In dieser Datei befinden sich die Methoden für die Generierung der HASH-Skripte aus dem ensprechenden Jinja-Template

# Methode zum Laden der Umgebung und des Templates
def loadEnv():
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/Hash-Template.txt')
    return template


# Methode zur Generierung eines HASH-Skriptes für die übergebene Tabelle
def execute(table):
    # Generierung des Scripts
    script = loadEnv().render(tablename=table.name, columns=table.getSelectedColumns())

    # Schreiben des Scripts
    with open("./Generierte Skripte/HASH-Skripte/" + table.name + "_HASH.sql", "w+") as f:
        f.write(script)


# Methode zur Generierung der Skripte für die übergebenen Tabellen
def executeAll(tables):
    for table in tables:
        execute(table)


# Methode zur Generierung einer .bat-Datei zum Ausführen der HASH-Skripte für die übergebenen Tabellen
def executeRunBatch(tables):
    # Laden des Templates
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/runHASH-Template.txt')

    # Generierung des Skripts
    script = template.render(tables=tables)

    # Schreiben des Skripts
    with open("Generierte Skripte/HASH-Skripte/runHASH.bat", "w+") as f:
        f.write(script)
