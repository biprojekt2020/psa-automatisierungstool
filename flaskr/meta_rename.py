from jinja2 import Environment, FileSystemLoader

# In dieser Datei befinden sich die Methoden für die Generierung der Skripte aus den ensprechenden Jinja-Templates
# für das umbenennen der CSV-Abzügen aus der DB2 sowie das Laden von Metadaten über die Dateien in Snowflake

# Methode zum Laden der Umgebung und des Templates
def loadEnv():
    env = Environment(loader=FileSystemLoader('.'))
    templatePUT = env.get_template('DWH-Templates/meta_PUT-Template.txt')
    templateCOPY = env.get_template('DWH-Templates/meta_COPYINTO-Template.txt')
    templateRENAME = env.get_template('DWH-Templates/rename-Template.txt')
    return templatePUT, templateCOPY, templateRENAME


# Methode zur Generierung eines meta_rename-Skriptes für die übergebene Tabelle
def execute(table):
    templatePUT, templateCOPY, templateRENAME = loadEnv()
    # Generierung des Scripts
    scriptPUT = templatePUT.render(tablename=table.name)
    scriptCOPY = templateCOPY.render(tablename=table.name)
    scriptRENAME = templateRENAME.render(tablename=table.name)

    # Schreiben der Skripte
    with open("./Generierte Skripte/RENAME-Skripte/" + table.name + "_PUT.sql", "w+") as f:
        f.write(scriptPUT)

    with open("./Generierte Skripte/RENAME-Skripte/" + table.name + "_COPYINTO.sql", "w+") as f:
        f.write(scriptCOPY)

    with open("./Generierte Skripte/RENAME-Skripte/" + table.name + "_rename.bat", "w+") as f:
        f.write(scriptRENAME)


# Methode zur Generierung der Skripte für die übergebenen Tabellen
def executeAll(tables):
    for table in tables:
        execute(table)

# Methode zur Generierung einer .bat-Datei zum Ausführen der meta_rename-Skripte für die übergebenen Tabellen
def executeRunBatch(tables):
    # Laden des Templates
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/runRENAME-Template.txt')

    # Generierung des Skripts
    script = template.render(tables=tables)

    # Schreiben des Skripts
    with open("Generierte Skripte/RENAME-Skripte/runRENAME.bat", "w+") as f:
        f.write(script)
