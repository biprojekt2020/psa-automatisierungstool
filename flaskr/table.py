from column import Column

# Objekte der Klasse Table repräsentieren eine Tabelle
class Table:
    # Name der Tabelle
    name = ""
    # Dictionary für die Spalten der Tabelle {Tabellenname: Tabellenobjekt}
    columns = {}
    # Attribut, ob die Tabelle ausgewählt ist
    isSelected = True

    def __init__(self, name):
        self.name = name
        self.columns = {}

    # Methode zum Hinzufügen einer Spalte zu der Tabelle
    def addColumn(self, name, datatype, length, nulls, isKey, isSourceKey, isSelected):
        self.columns[name] = Column(name, datatype, length, nulls, isKey, isSourceKey, isSelected)

    # Übergebene Spalte als Key markieren
    def addKey(self, k):
        self.columns.get(k).isKey = True

    # Keys zurücksetzen
    def resetKeys(self):
        for c in self.columns.values():
            c.isKey = False

    # Übergebene Spalte als SourceKey markieren
    def addSourceKey(self,  k):
        self.columns.get(k).isSourceKey = True

    # Methode, die Informationstext zum Überprüfen der Schlüssel zurückgibt
    def checkKeyType(self):
        sourcekeys = self.getSourceKeyNames()
        # Wenn die SourceKey-Liste nicht leer ist
        if sourcekeys:
            # Wenn alle Elemente der SourceKey-Liste in der Auswahl-Liste sind
            if all(column in self.getSelectedColumnNames() for column in sourcekeys):
                return "Alle Source-Keys sind in der User-Auswahl"
            # Wenn mindestens ein Element der SourceKey-Liste in der Auswahl-Liste ist
            elif any(column in self.getSelectedColumnNames() for column in sourcekeys):
                return "Teile der Source-Keys sind in der User-Auswahl"
            # Kein Element der SourceKey-Liste ist in der Auswahl-Liste
            else:
                return "Kein Source-Key ist in der User-Auswahl"
        # Wenn die SourceKey-Liste leer ist
        else:
            return "Source-Keys nicht in der CSV definiert"

    # Methode, die zurückgibt, ob der User die Schlüssel überprüfen muss
    def checkKey(self):
        sourcekeys = self.getSourceKeyNames()
        # Wenn die SourceKey-Liste nicht leer ist
        if sourcekeys:
            # Wenn alle Elemente der SourceKey-Liste in der Auswahl-Liste sind
            if all(column in self.getSelectedColumnNames() for column in sourcekeys):
                return False
            # Wenn mindestens ein Element der SourceKey-Liste in der Auswahl-Liste ist
            elif any(column in self.getSelectedColumnNames() for column in sourcekeys):
                return True
            # Kein Element der SourceKey-Liste ist in der Auswahl-Liste
            else:
                return True
        # Wenn die SourceKey-Liste leer ist
        else:
            return True

    # Methode zur Auswahl der Tabelle
    def select(self):
        self.isSelected = True

    # Methode zur Abwahl der Tabelle
    def deselect(self):
        self.isSelected = False

    # Methode zum Zurücksetzen der Auswahl für alles Spalten der Tabelle
    def resetSelection(self):
        for column in self.columns.values():
            column.deselect()

    # Methode, die eine Liste mit allen ausgewählten Spalten der Tabelle zurückgibt
    def getSelectedColumns(self):
        c = []
        for column in self.columns.values():
            if column.isSelected:
                c.append(column)
        return c

    # Methode, die eine Liste mit den Namen allen ausgewählten Spalten der Tabelle zurückgibt
    def getSelectedColumnNames(self):
        c = []
        for column in self.columns.values():
            if column.isSelected:
                c.append(column.name)
        return c

    # Methode, die eine Liste mit den Namen aller Schlüsselspalten der Tabelle zurückgibt
    def getKeyNames(self):
        k = []
        for column in self.columns.values():
            if column.isKey and column.isSelected:
                k.append(column.name)
        return k

    # Methode, die eine Liste mit den Namen aller Schlüsselspalten der Tabelle aus der SYS_COLUMS-Datei zurückgibt
    def getSourceKeyNames(self):
        k = []
        for column in self.columns.values():
            if column.isSourceKey:
                k.append(column.name)
        return k
