from jinja2 import Environment, FileSystemLoader

# In dieser Datei befinden sich die Methoden für die Generierung der Export-Skripte aus dem ensprechenden Jinja-Template

# Methode zum Laden der Umgebung und des Templates
def loadEnv():
    # Laden des Templates
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/Export-Table-Template.txt')
    return template


# Methode zur Generierung einer .bat-Datei zum Exportieren der übergebenen Tabelle aus der DB2
def execute(table):
    # Generierung des Scripts
    script = loadEnv().render(tablename=table.name, columns=table.getSelectedColumns())

    # Schreiben des Scripts
    with open("./Generierte Skripte/DB2-Export-Skripte/" + table.name + "_Export.bat", "w+") as f:
        f.write(script)


# Methode zur Generierung der Skripte für die übergebenen Tabellen
def executeAll(tables):
    for table in tables:
        execute(table)


# Methode zur Generierung einer .bat-Datei zum Ausführen der Exporte für die übergebenen Tabellen
def executeRunBatch(tables):
    # Laden des Templates
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/runExport-Template.txt')

    # Generierung des Skripts
    script = template.render(tables=tables)

    # Schreiben des Skripts
    with open("Generierte Skripte/DB2-Export-Skripte/runExport.bat", "w+") as f:
        f.write(script)
