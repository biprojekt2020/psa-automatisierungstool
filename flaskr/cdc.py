from jinja2 import Environment, FileSystemLoader

# In dieser Datei befinden sich die Methoden für die Generierung der CDC-Skripte aus dem ensprechenden Jinja-Template

# Methode zum Laden der Umgebung und des CDC-Templates
def loadEnv():
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/CDC-Template.txt')
    return template


# Methode zur Generierung eines CDC-Skriptes  für die übergebene Tabelle
def execute(table):
    # Generierung des Skripts
    script = loadEnv().render(tablename=table.name, columns=table.getSelectedColumns(), keys=table.getKeyNames())

    # Schreiben des Skripts
    with open("./Generierte Skripte/CDC-Skripte/" + table.name + "_CDC.sql", "w+") as f:
        f.write(script)


# Methode zur Generierung der Skripte  für die übergebenen Tabellen
def executeAll(tables):
    for table in tables:
        execute(table)


# Methode zur Generierung einer .bat-Datei zum Ausführen der CDC-Skripte für die übergebenen Tabellen
def executeRunBatch(tables):
    # Laden des Templates
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/runCDC-Template.txt')

    # Generierung des Skripts
    script = template.render(tables=tables)

    # Schreiben des Skripts
    with open("Generierte Skripte/CDC-Skripte/runCDC.bat", "w+") as f:
        f.write(script)
