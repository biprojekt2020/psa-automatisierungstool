# Objekte der Klasse Column repräsentieren eine Spalte 

class Column:
    name = ""
    datatype = ""
    length = ""
    nullable = ""
    # Attribut, ob die Spalte ein Schlüsselfeld ist (kann vom Nutzer verändert werden)
    isKey = False
    # Attribut, ob die Spalte in der SYS_COLUMSN-Datei als Schlüsselfeld markiert ist (kann nicht vom Nutzer verändert werden)
    isSourceKey = False
    # Attribut, ob die Spalte von Nutzer ausgewählt wurde
    isSelected = True

    def __init__(self, name, datatype, length, nullable, isKey, isSourceKey, isSelected):

        # Standardisierung von Datenbank-Typen
        if datatype == "CHAR":
            standardtype = "VARCHAR"
        elif datatype == "BIGINT" or datatype == "SMALLINT" or datatype == "DECIMAL":
            standardtype = "INTEGER"
            length = 38
        else:
            standardtype = datatype

        if (standardtype == "VARCHAR") and length <= 100:
            standardlength = 100
        elif (standardtype == "VARCHAR") and length <= 1000:
            standardlength = 1000
        elif (standardtype == "VARCHAR") and length > 1000:
            standardlength = 4000
        else:
            standardlength = length

        # Aktuell soll jede Spalte nullable sein, soll dies nicht mehr der Fall sein muss der
        # Kommentar um den nachfolgenden Abschnitt entfernt und die nachfolgende Zeile kommentiert werden
        '''
        if nullable == 'N':
            standardnullable = "not null"
        elif nullable== 'Y':
            standardnullable = "null"
        else:
            standardnullable = nullable
        '''
        standardnullable = "null"

        self.name = name
        self.datatype = standardtype
        self.length = standardlength
        self.nullable = standardnullable
        self.isKey = isKey
        self.isSourceKey = isSourceKey
        self.isSelected = isSelected

    # Methode zur Auswahl der Spalte
    def select(self):
        self.isSelected = True

    # Methode zur Abwahl der Spalte
    def deselect(self):
        self.isSelected = False

    '''
    def startCall():
        pass

    def endCall():
        pass
    '''
