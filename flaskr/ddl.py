from jinja2 import Environment, FileSystemLoader

# In dieser Datei befinden sich die Methoden für die Generierung der beiden DDL-Skripte aus den ensprechenden Jinja-Templates

# Methode zum Laden der Umgebung und der Templates für die Landing Zone und die Persistent Staging Area
def loadEnv():
    env = Environment(loader=FileSystemLoader('.'))
    templateLZ = env.get_template('DWH-Templates/DDL-LZ-Template.txt')
    templatePSA = env.get_template('DWH-Templates/DDL-PSA-Template.txt')
    return templateLZ, templatePSA


# Methode zur Generierung der verschiedenen DDL-Skripte  für die übergebene Tabelle
def execute(table):
    templateLZ, templatePSA = loadEnv()
    # Generierung der Skripte
    scriptLZ = templateLZ.render(tablename=table.name, columns=table.getSelectedColumns())
    scriptPSA = templatePSA.render(tablename=table.name, columns=table.getSelectedColumns())

    # Schreiben der Scripte
    with open("./Generierte Skripte/DDL-Skripte/Landing_Area/" + table.name + "_DDL.sql", "w+") as f:
        f.write(scriptLZ)
    with open("./Generierte Skripte/DDL-Skripte/PSA/" + table.name + "_DDL.sql", "w+") as f:
        f.write(scriptPSA)


# Methode zur Generierung der Skripte  für die übergebenen Tabellen
def executeAll(tables):
    for table in tables:
        execute(table)


# Methode zur Generierung einer .bat-Datei zum Ausführen der DDL-Skripte für die übergebenen Tabellen
def executeRunBatch(tables):
    # Laden des Templates
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/runDDL-Template.txt')

    # Generierung des Skripts
    script = template.render(tables=tables)

    # Schreiben des Skripts
    with open("Generierte Skripte/DDL-Skripte/runDDL.bat", "w+") as f:
        f.write(script)
