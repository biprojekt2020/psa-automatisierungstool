from jinja2 import Environment, FileSystemLoader

# In dieser Datei befinden sich die Methoden für die Generierung der Skripte
# für das Generieren der Metadaten über die Ladeläufe aus dem ensprechenden Jinja-Template

# Methode zum Laden der Umgebung und des Templates
def loadEnv():
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/meta_ladeläufe-Template.txt')
    return template


# Methode zur Generierung eines meta_ladelauf-Skriptes für die übergebene Tabelle
def execute(table):
    # Generierung des Skripts
    script = loadEnv().render(tablename=table.name)

    # Schreiben des Skripts
    with open("./Generierte Skripte/meta-Ladelauf-Skripte/" + table.name + "_METALADELAUF.sql", "w+") as f:
        f.write(script)


# Methode zur Generierung der Skripte für die übergebenen Tabellen
def executeAll(tables):
    for table in tables:
        execute(table)


# Methode zur Generierung einer .bat-Datei zum Ausführen der meta-Ladelauf-Skripte für die übergebenen Tabellen
def executeRunBatch(tables):
    # Laden des Templates
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/runMETALADELAUF-Template.txt')

    # Generierung des Skripts
    script = template.render(tables=tables)

    # Schreiben des Skripts
    with open("Generierte Skripte/meta-Ladelauf-Skripte/runMETALADELAUF.bat", "w+") as f:
        f.write(script)
