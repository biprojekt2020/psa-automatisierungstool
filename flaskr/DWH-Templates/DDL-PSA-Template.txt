
{#- Erstellen/Ersetzten der Tabelle #}
create table "STUD_FH_DB"."HISTORICAL_ARCHIVE".{{tablename}} (
  CDCOPERATION VARCHAR(100) not null,
{#- Für jede ausgewählte Spalte wird hier Name, Datentyp und Nullable Attribute eingesetzt #}
{%- for c in columns %}
  {{c.name}} {{c.datatype}} {{c.nullable}},
{%- endfor %}
  LOADDATE TIMESTAMP null default current_timestamp,
  FULLROWHASH VARCHAR(100) null
);