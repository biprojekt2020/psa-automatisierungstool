

{#- Nachdem der Copy-Into-Batch die Inhalte der CSV-Datei in die dazugehörige Landing Zone Tabelle geladen hat,
   wird mit nachstehendem Statement ein Update auf die entsprechende Tabelle durchgeführt, um mit der internen Hash-Funktion von Snowflake (HASH(SPALTEN)) die Spalte FULLROWHASH zu füllen.
   Die vorligende Hash-Funktion muss mit einer PostgreSQL kompatiblen Hsh-Funktion ersetzt werden.
   Die Funktion HASH(SPALTEN) gibt einen 64-Bit Hash-Wert zurück, auch wenn Teile der Spalten NULL sind oder alle Spalten einen NULL-Value besitzen.
   WICHTIG: Man hat keine Möglichkeit den verwendeten Algorithmus zu verändern!
   Diese Operation soll ausschließlich für die Zeilen durchgeführt werden, bei denen der FULLROWHASH noch nicht gefüllt ist.
   #}
update "STUD_FH_DB"."LANDING_ZONE".{{tablename}}
set FULLROWHASH = HASH({%for c in columns%} {{c.name}}{%if not loop.last%},{%endif%}{%endfor%})
where FULLROWHASH IS NULL;