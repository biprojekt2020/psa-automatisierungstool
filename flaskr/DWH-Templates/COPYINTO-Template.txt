
{#- Vorhandene Daten in Landing Zone löschen #}
truncate {{tablename}};
{#-
   Die Bestandteile des Copy-Into-Befehls sind zum einen der Tabellenname mit allen dazugehörigen Spalten sowie ein eingebettetes SELECT-Statement auf die Spalten der CSV.GZ-Datei.
   WICHTIG: Es muss hier jede Spalte aufgeführt werden (t.$1 .. t.$n), da sonst die Inhalte nicht in die richtigen Spalten geschrieben werden oder diverse MISMATCH-Fehlermeldungen auftauchen.
   Zusätzlich ist es zwinged erforderlich das richtige, zuvor in PostgreSQL definierte, FILE_FORMAT anzugeben. Im FILE_FORMAT wird festgehalten, welche Art von CSV-Delimiter gegeben ist.
#}


\copy "LANDING_ZONE".{{tablename}}({%for c in columns%} {{c.name}}{%if not loop.last%},{%endif%}{%endfor%}) from 'C:\Users\db2admin\Desktop\ABZUG_DB2\{{tablename}}.csv'  with (format csv, delimiter E'\t', quote '"');

