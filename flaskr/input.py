import pandas, math
from table_container import TableContainer

# In diesem File stehen Methoden zum Laden und Einlesen der SYS_COLUMNS CSV-Datei in die Datenstruktur

# Liste der vorhandene Spaltennamen von sys_columns angeben
colnamesSysColumns = ['NAME', 'TBNAME', 'TBCREATOR', 'COLNO', 'COLTYPE', 'LENGTH', 'SCALE', 'NULLS', 'COLARD', 'REMARKS',
                      'DEFAULT', 'KEYSEQ', 'TYPESCHEMA', 'TYPENAME', 'HIDDEN']

# Methode zum Einlesen der CSV
def readSYS():
    # Einlesen der SYS_COLUMNS-Datei
    sys_columns = pandas.read_csv('./DB2/SYS_COLUMNS.csv', names=colnamesSysColumns, delimiter='\t')
    return sys_columns

# Methode zum Verarbeiten der CSV-Datei
def computeSYS(file):
    # Temporärer Container
    container = TableContainer()
    # File, welches der Methode als Parameter übergeben wurde
    sys_columns = file
    # Verarbeiten und Erstellen der Tabellen + Zuordnung der Spalten und Keys
    for index, row in sys_columns.iterrows():
        # Finden der Tabelle zu der aktuellen Zeile der SYS_COLUMS-Datei, wenn nicht vorhanden wird sie erstellt
        current_table = container.findTable(row['TBNAME'])
        # Die aktuelle Zeile der SYS_COLUMS-Datei wird zuvor gefundenen Tabelle als Spalte hinzugefügt
        # Außerdem wird die Spalte als zunächst nicht als Key/SourceKey hinzugefügt, jedoch als ausgewählt markiert
        current_table.addColumn(row['NAME'], row['COLTYPE'].strip(), row['LENGTH'], row['NULLS'], False, False, True)

        # Wenn die Spalte KEYSEQ der SYS_COLUMS-Datei einen Wert hat, wird die aktuelle Zeile als Key und SourceKey markiert
        if not math.isnan(row['KEYSEQ']):
            container.findTable(row['TBNAME']).addKey(row['NAME'])
            container.findTable(row['TBNAME']).addSourceKey(row['NAME'])
    # Rückgabe des Containers
    return container

# Methode zum vollständigen Einlesen und Verarbeiten
def complete():
    # Temporärer Container
    container = TableContainer()
    # Einlesen von SYS_COLUMNS
    sys_columns = readSYS()
    # Verarbeiten mit dem Ergebnis als Container
    container = computeSYS(sys_columns)
    # Rückgabe des Containers
    return container