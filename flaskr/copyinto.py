from jinja2 import Environment, FileSystemLoader

# In dieser Datei befinden sich die Methoden für die Generierung der COPY INTO-Skripte aus dem ensprechenden Jinja-Template

# Methode zum Laden der Umgebung und des Templates
def loadEnv():
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/COPYINTO-Template.txt')
    return template


# Methode zur Generierung eines COPY INTO-Skriptes für die übergebene Tabelle
def execute(table):
    # Generierung des Skripts
    script = loadEnv().render(tablename=table.name, columns=table.getSelectedColumns())

    # Schreiben des Skripts
    with open("./Generierte Skripte/COPYINTO-Skripte/" + table.name + "_COPYINTO.sql", "w+") as f:
        f.write(script)


# Methode zur Generierung der Skripte für die übergebenen Tabellen
def executeAll(tables):
    for table in tables:
        execute(table)


# Methode zur Generierung einer .bat-Datei zum Ausführen der COPY INTO-Skripte für die übergebenen Tabellen
def executeRunBatch(tables):
    # Laden des Templates
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('DWH-Templates/runCOPYINTO-Template.txt')

    # Generierung des Scripts
    script = template.render(tables=tables)

    # Schreiben des Scripts
    with open("Generierte Skripte/COPYINTO-Skripte/runCOPYINTO.bat", "w+") as f:
        f.write(script)
